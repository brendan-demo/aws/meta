# Howdy Partner stream

2020-05-06

## Agenda
1. Intro: What are we building
2. Getting everything set up
3. Start a new project
4. Deploy with SAM CLI
5. Upload to GitLab
6. Setup GitLab CI/CD
7. Do more by creating a "quine" 
?. ?
?. Profit

## 1) Intro

...

## 2) Getting everything setup

* Install AWS CLI, SAM CLI, Docker
* AWS 

## 3) Start a new project

* Take a look at CLI options with `sam`
* `sam init`
* cd into new folder
* Browse code
* Run Hello World locally

`sam local invoke HelloWorldFunction --event events/event.json`

* Run local API

`sam local start-api`

* Hit local endpoint

`curl localhost:3000/hello | jq`

## 4) Deploy with SAM CLI

* Build the package 

`sam build`

* Deploy to AWS guided

`sam deploy --guided`

* Walk through the deploy steps and wait for the deploy to finish
* Discuss what it's doing
* Checkout live function
* Checkout AWS console

## 5) Upload the project to GitLab
* Initalize a new git repository 

`git init`

* Note the .gitignore file to ignore build resources
* Add this line to the .gitignore to ignore built files

`.aws-sam/`

* Add and commit the files

`git add .`
`git commit -m "First commit"`

* Add a remote repo 

`git remote add origin git@gitlab.com:brendan-demo/aws/sam.git`

* Push to the remote

`git push`

* Explore the repo on GitLab
* Make it public for folks to follow along on Twitch

## 6) Setup GitLab CI/CD
* Open WebIDE
* Add .gitlab-ci.yml 
* Start from docker image

`image: nikolaik/python-nodejs:python3.8-nodejs12-alpine`

* Install SAM CLI

```
apk add --no-cache --virtual builddeps gcc musl-dev 
pip --no-cache-dir install aws-sam-cli awscli
```

* Build and deploy with SAM

```
sam build
sam deploy
```

* Oh wait!  What about auth?!
* Add in API keys as GitLab CI/CD variables

```
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
```


## 7) My version of a quine

Add the ability to get the result of pipeline results from the app the pipeline deploys

* `npm i --save axios`
* Add axios required
* Get pipeline status from GitLab API:

```
const ret = await axios(`https://gitlab.com/api/v4/projects/${projectId}/pipelines?order_by=updated_at`); 
```

* Return that data
* So meta!
